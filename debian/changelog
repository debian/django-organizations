django-organizations (2.3.1-1) unstable; urgency=medium

  * Orphan the package, see #1060406
    - Remove myself from uploaders and update maintainer to Debian QA Group
    - Update Vcs-* to Debian group
  * Add d/source/options extend-diff-ignore to fix dpkg-source failure due to
    local changes (python package metadata regeneration) (Closes: #1044258)
  * Bump standards-version to 4.6.2 without further change
  * New upstream release

 -- Scott Kitterman <scott@kitterman.com>  Wed, 10 Jan 2024 14:36:31 -0500

django-organizations (2.1.0-1) unstable; urgency=medium

  * New upstream release
  * Bump standards-version to 4.6.1 without further change
  * Update debian/watch to version 4
  * Update debian/copyright file path due to upstream reorganization
  * Add Bug-Submit field to debian/upstream/metadata

 -- Scott Kitterman <scott@kitterman.com>  Fri, 28 Oct 2022 09:07:12 -0400

django-organizations (2.0.1-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Set upstream metadata fields: Repository, Repository-Browse.

  [ Scott Kitterman ]
  * New upstream release
  * Update debian/copyright
  * Update debhelper-compat to 13
  * Bump standards-version to 4.6.0 without further change

 -- Scott Kitterman <scott@kitterman.com>  Mon, 06 Dec 2021 19:15:20 -0500

django-organizations (1.1.2-1) unstable; urgency=medium

  * New upstream release
  * Bump standards-version to 4.4.0 without further change
  * Fix previous d/changelog entry
  * Finish python2 removal

 -- Scott Kitterman <scott@kitterman.com>  Sun, 04 Aug 2019 18:25:18 -0400

django-organizations (1.1.1-2.1) unstable; urgency=medium

  * Team upload.

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.

  [ Thomas Goirand ]
  * Removed Py2 support.

 -- Thomas Goirand <zigo@debian.org>  Thu, 25 Jul 2019 05:41:29 +0200

django-organizations (1.1.1-1) unstable; urgency=medium

  * New upstream release
  * Bump standards-version to 4.2.1 without further change
  * Bump debiam/compat and debhelper min versionn to 10

 -- Scott Kitterman <scott@kitterman.com>  Tue, 11 Dec 2018 08:06:47 -0500

django-organizations (1.1.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * d/watch: Use https protocol

  [ Scott Kitterman ]
  * New upstream release
  * Bump standards-version to 4.1.4 without further change

 -- Scott Kitterman <scott@kitterman.com>  Sun, 20 May 2018 01:32:50 -0400

django-organizations (1.0.0-1) unstable; urgency=medium

  * New upstream release
  * Bump standards-version to 4.1.3 without further change

 -- Scott Kitterman <scott@kitterman.com>  Mon, 08 Jan 2018 15:00:57 -0500

django-organizations (0.9.2-1) unstable; urgency=medium

  * New upstream release
  * Bump standards version to 4.0.1 without further change

 -- Scott Kitterman <scott@kitterman.com>  Mon, 07 Aug 2017 01:24:10 -0400

django-organizations (0.9.1-1) unstable; urgency=medium

  * New upstream release

 -- Scott Kitterman <scott@kitterman.com>  Tue, 20 Jun 2017 17:34:16 -0400

django-organizations (0.8.2-1) unstable; urgency=medium

  * Initial release. (Closes: #843915)

 -- Scott Kitterman <scott@kitterman.com>  Thu, 10 Nov 2016 12:39:49 -0500
